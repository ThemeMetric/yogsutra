<?php
    if ( ! class_exists( 'Redux' ) ) {
        return;
    }
    $opt_name = "yoga_opt";
    $theme = wp_get_theme(); // For use with some settings. Not necessary.
    $args = array(
        'display_name'         => $theme->get( 'Name' ),
        'display_version'      => $theme->get( 'Version' ),
	'dev_mode'             => FALSE,
        'menu_title'           => __( 'Yogsutra Options', 'yogsutra' ),
	'page_title'           => __( 'Yogsutra Options', 'yogsutra' ),
	 'menu_type'            => 'menu',
	'footer_credit'     => 'Yogsutra',
	'allow_sub_menu'       => FALSE,
	 'update_notice'        => FALSE,
	 'admin_bar'            => FALSE,
        'customizer'           => true
    );

    Redux::setArgs( $opt_name, $args );
    Redux::setSection( $opt_name, array(
        'title'  => __( 'General', 'yogsutra' ),
        'id'     => 'general',        
        'icon'   => 'el el-home',
        'fields' => array(       
	     array (
                'title' => __('Favicon', 'yogsutra'),            
                'id' => 'favicon',
                'type' => 'media',
		   'hint'     => array(
                    'content' => '<em>Upload your custom Favicon image. <br>.ico or .png file required.</em>',
                )
              
            )
	    
        )
    ) );
    
    // Header Start 
    Redux::setSection( $opt_name, array(
        'icon'   => 'el el-laptop',
        'title'  => __( 'Header', 'yogsutra' ),
        'fields' => array(           
        ),
        
    ) );
    
         Redux::setSection( $opt_name, array(
       
        'title'    => __( 'Logo', 'yogsutra' ),
        'subsection' => true,
        'fields'     => array(
        
         array (
                'title' => __('Upload Logo', 'yogsutra'),            
                'id' => 'logo',
                'type' => 'media',
		   'hint'     => array(
                    'content' => '<em>Upload your custom logo. <br>jpg or .png file required.</em>',
                )
              
            ),
	   
        )
        
    ) );
	 
	       Redux::setSection( $opt_name, array(
       
        'title'    => __( 'Header Banner', 'yogsutra' ),
        'subsection' => true,
        'fields'     => array(
        
         array (
                'title' => __('Header Banner', 'yogsutra'),            
                'id' => 'header-banner',
                'type' => 'media',
		   'hint'     => array(
                    'content' => '<em>Upload your header banner images.</em>',
                )
              
            ),
	       array (
                'title' => __('Header banner link', 'yogsutra'),            
                'id' => 'header-banner-link',
                'type' => 'text',
		   'hint'     => array(
                    'content' => '<em>Add your header banner link</em>',
                )
              
            ),
	   
        )
        
    ) );
	 
	     
// Header end	     
// blog post 
  	    
Redux::setSection( $opt_name, array(
    'title'  => __( 'Blog post settings', 'yogsutra' ),
    'id'     => 'advanced-blog',        
    'icon'   => 'el el-leaf',
    'fields' => array(       
  
    array(
        'id'       => 'opt-switch',
        'type'     => 'switch', 
        'title'    => __('Signle post - Related post show', 'yogsutra'),
        'subtitle' => __('Look, it\'s on!', 'yogsutra'),
        'default'  => true,
            
        ),
               
                    
    )
) );    

// Start Advanced option 

    Redux::setSection( $opt_name, array(
        'title'  => __( 'Advanced Option', 'yogsutra' ),
        'id'     => 'advanced',        
        'icon'   => 'el el-adjust',
        'fields' => array(       
	  
	    array(
                'id'       => 'analytics_js',
                'type'     => 'ace_editor',
		'mode'     => 'javascript',
                'theme'    => 'monokai',
                'title'    => __( 'Google Analytics  Code', 'yogsutra' ),
		'subtitle' => __( 'Paste your JS code here.', 'yogsutra' ),
                'hint'     => array(
                    'content' => '<em>Add your Google analytics code here.</em>',
                )
                
            ),
            
            	    	    array(
                'id'       => 'single_post_top_add_js',
                'type'     => 'ace_editor',
                'mode'     => 'javascript',
                'theme'    => 'monokai',
                'title'    => __( 'Single post top content add  Code', 'yogsutra' ),
		'subtitle' => __( 'Paste your add JS code here.', 'yogsutra' ),
                'hint'     => array(
                    'content' => '<em>Add your add code here.</em>',
                )
                
            ),
            
            	    	    	    array(
                'id'       => 'single_post_bottom_add_js',
                'type'     => 'ace_editor',
                'mode'     => 'javascript',
                'theme'    => 'monokai',
                'title'    => __( 'Single post bottom   add  Code', 'yogsutra' ),
		'subtitle' => __( 'Paste your add JS code here.', 'yogsutra' ),
                'hint'     => array(
                    'content' => '<em>Add your add code here.</em>',
                )
                
            ),
	    
	       array(
                'id'       => 'custom-css',
                'type'     => 'ace_editor',
                'title'    => __( 'CSS Code', 'yogsutra' ),
                'subtitle' => __( 'Paste your CSS code here.', 'yogsutra' ),
                'mode'     => 'css',
                'theme'    => 'monokai',
		   'hint'     => array(
                    'content' => '<em>Add your custom css code here.</em>',
                )
               
            ),
            		    
        )
    ) );	 
    
    // End  Advanced option 
    // social link
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Solcial', 'yogsutra' ),
        'id'     => 'social',
	'desc'     => 'Top bar right social link',       
        'icon'   => 'el el-address-book',
        'fields' => array(       
	  
	      array(
                'id'       => 'yogsutra-facebook',
                'type'     => 'text',
                'title'    => __( 'Facebook', 'yogsutra' ),            
                'hint'     => array(
                    'content' => 'Add facebook profile link here',
                )
	    ),
	      array(
                'id'       => 'yogsutra-twitter',
                'type'     => 'text',
                'title'    => __( 'Twitter', 'yogsutra' ),            
                'hint'     => array(
                    'content' => 'Add twitter profile link here',
                )
	    ),
	      array(
                'id'       => 'yogsutra-linkdin',
                'type'     => 'text',
                'title'    => __( 'Linkdin', 'yogsutra' ),            
                'hint'     => array(
                    'content' => 'Add linkdin profile link here',
                )
	    ),
	      array(
                'id'       => 'yogsutra-google',
                'type'     => 'text',
                'title'    => __( 'Google Plus', 'yogsutra' ),            
                'hint'     => array(
                    'content' => 'Add Google Plus profile link here',
                )
	    )
	    
        )
    ) );
    
    // Start Footer  option 

    Redux::setSection( $opt_name, array(
        'title'  => __( 'Footer', 'yogsutra' ),
        'id'     => 'footer',        
        'icon'   => 'el el-edit',
        'fields' => array(       
	  
	      array(
                'id'       => 'footer-copyright',
                'type'     => 'text',
                'title'    => __( 'Footer Copy right text', 'yogsutra' ),            
                'hint'     => array(
                    'content' => 'Add your footer copyright text.',
                )
	    )   
	    
        )
    ) );	 
    
    // End  Footer option 
	 