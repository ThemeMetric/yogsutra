<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Yogsutra
 */

get_header(); ?>
<div class="container">
    <div class="row">
           <div id="primary" class="content-area">
	<main id="main" class="site-main">	
	           <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
		    <?php
		    if ( have_posts() ) :
			    if ( is_home() && ! is_front_page() ) : ?>
			    <header>
			    <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
			    </header>
			    <?php
			    endif;
			    /* Start the Loop */
			    while ( have_posts() ) : the_post();
				    /*
				     * Include the Post-Format-specific template for the content.
				     * If you want to override this in a child theme, then include a file
				     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				     */
				    get_template_part( 'template-parts/content', get_post_format() );

			    endwhile;			
		    else :
		    get_template_part( 'template-parts/content', 'none' );
		    endif; ?>		    
		    <div class="post-pagination text-center"> 
		      <?php the_posts_pagination(array(
			'next_text' => '<span aria-hidden="true">Next <i class="fa fa-angle-right" aria-hidden="true"></i></span>',
			'prev_text' => '<span aria-hidden="true"> <i class="fa fa-angle-left" aria-hidden="true"></i> Prev </span>',
			'screen_reader_text' => ' ',
			'type'                => 'list'
			)); ?>
		    </div> 		    		    
	           </div>
	           <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">	    
		 <?php get_sidebar(); ?>	    
	          </div>		    
	  </main><!-- #main -->
	</div><!-- #primary -->	
    </div> 
</div>
<?php get_footer();
