<?php

  /*
  Template Name: Home without slider 
  
  */

global $yoga_opt;
get_header(); ?>
<div class="container">
    <div class="row">
	<div id="primary" class="content-area">	    	    
	          <main id="main" class="site-main">		    		
                                    <div class="col-md-8 col-lg-8 col-sm-6 col-xs-12">    
		    <?php  while ( have_posts() ) : the_post();		
                                                        the_content();
		             endwhile; ?>
	             </div>
                                  <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">	    
	                           <?php get_sidebar(); ?>	    
	            </div>		    
	  </main><!-- #main -->
	</div><!-- #primary -->	
    </div> 
</div>
<?php get_footer();