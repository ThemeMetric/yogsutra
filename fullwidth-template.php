<?php

  /*
  Template Name: Home Full Width
  
  */

global $yoga_opt;
get_header(); ?>
<div class="container">
    <div class="row">
	<div id="primary" class="content-area">	    	    
	          <main id="main" class="site-main">		    		
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">    
		    <?php  while ( have_posts() ) : the_post();		
                                                        the_content();
		             endwhile; ?>
	             </div>
                               	    
	  </main><!-- #main -->
	</div><!-- #primary -->	
    </div> 
</div>
<?php get_footer();