<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Yogsutra
 */
global $yoga_opt;
?>
</div><!-- #content -->
	<section class="upper-footer">
	    <div class="container footer-widget">
		<div class="row">
		    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">		   
			<aside id="secondary" class="widget-area">
	                                                  <?php dynamic_sidebar( 'footer-1' ); ?>
                                                                  </aside>					                   	
		    </div>
		    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12"> 
			<aside id="secondary" class="widget-area">
	                                                     <?php dynamic_sidebar( 'footer-2' ); ?>
                                                                    </aside>
		    </div>
		    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
			<aside id="secondary" class="widget-area">
	                                                       <?php dynamic_sidebar( 'footer-3' ); ?>
                                                                  </aside>
		    </div>
		    <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12"> 
		    <aside id="secondary" class="widget-area">
	                                             <?php dynamic_sidebar( 'footer-4' ); ?>
                                                </aside>
		    </div>		   		
		</div>
	      </div>
	</section>          	
<div class="scroll-btn-container">			
     <div style="display:none;" class="scroll-to-top" id="scroll-to-top">	 
	 <i class="fa fa-angle-up"></i>	
     </div>
    <div style="display:block;" class="scroll-to-bottom" id="scroll-to-bottom">	
	<i class="fa fa-angle-down"></i>       
    </div>				
</div>
	<footer id="colophon" class="site-footer">	    	    
	    <div class="container bottom-footer">
		<div class="row"> 		
		    <div class="site-info text-center">			
			<?php if($yoga_opt['footer-copyright']) :?>			
			<p><?php echo $yoga_opt['footer-copyright'] ; ?></p>			
			<?php else: ?>			
			<?php endif ?>
		    </div>		
		</div>
	    </div>	    	
	</footer>
	
<section class="mobile-footer-banner"> 

<img class="img-responsive" src="http://www.yogsutra.com/wp-bdtg/uploads/2017/08/footer-banner.gif" alt="Yogsutra" />

</section>
	
	
	
	
    </div>
<?php wp_footer(); ?>
  </body>
</html>
