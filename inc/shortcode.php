<?php

// Enable shortcodes in text widgets
add_filter('widget_text', 'do_shortcode');

// cat shortcode 
add_shortcode('cat_post', function($attr, $content){
	ob_start(); 
extract( shortcode_atts(array(
	'title' => 'Categories Title',
        'category' => ' Uncategorized',
        'right_post_count' => 6
), $attr) );

?>
<div class="cat-post-sec">
<div class="home-title-content">
<h2 class="home-title"><span><?php echo $title ?></span></h2>	
</div>		    
 <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">			
   <?php $queryObject = new  Wp_Query( array(
    'showposts' => 1,
    'post_type' => array('post'),
    'category_name' => $category,
       'order'            => 'DESC'
    ));

// The Loop
if ( $queryObject->have_posts() ) : 
    while ( $queryObject->have_posts() ) :
        $queryObject->the_post();
      ?>
            <div class="first-post">
            <div class="post-media">               
	     <a href="<?php the_permalink(); ?>">
		<?php if ( has_post_thumbnail() ) {
                  the_post_thumbnail( 'post', array( 'class'  => 'img-responsive' ) );
                 } 
	?></a>				
          </div>  
                      <header class="entry-header">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		 ?>
	</header><!-- .entry-header -->

	<div class="entry-content entry-content-link-button">
	    
	   <?php the_excerpt(); ?>
	    
	   <a href="<?php echo get_permalink(); ?>">Read More</a>	    
	   	
	</div><!-- .entry-content -->
		
	
		
             </div>
        
   <?php endwhile;
endif; 
wp_reset_postdata();
?>
			
 </div>

<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
	    
   <?php $queryObject = new  Wp_Query( array(
    'showposts' => $right_post_count,
    'post_type' => array('post'),
    'category_name' => $category,
       'order'            => 'ASC'
    ));

// The Loop
if ( $queryObject->have_posts() ) : 
    while ( $queryObject->have_posts() ) :
        $queryObject->the_post();
      ?>	    
 <div class="postlist">
<ul class="media-list">
<li class="media"><a href="<?php the_permalink(); ?>">
		<?php if ( has_post_thumbnail() ) {
                  the_post_thumbnail( 'post-img-sidebar', array( 'class'  => 'img-responsive' ) );
                 } 
	?></a>
<h3><a href="<?php the_permalink();?>" rel="bookmark"><?php the_title();?></a></h3>
</li>
</ul>
</div>
<?php endwhile;
endif; 
wp_reset_postdata();
?>	    
	    
	</div> 	 
	</div> 
<?php return ob_get_clean();
});

/* shortcode for blank line */
function blankline() {
 return '<p>&nbsp;';
}
add_shortcode('blank', 'blankline');


// About one section shortcode

add_shortcode('recent_comments', function($attr, $content){
	ob_start(); 
extract( shortcode_atts(array(
	'title' => '',	          
	
), $attr) );

?>
<?php 

// Recent Comments function  with image

function bg_recent_comments($no_comments = 6, $comment_len = 30, $avatar_size = 50) {
	$comments_query = new WP_Comment_Query();
	$comments = $comments_query->query( array( 'number' => $no_comments ) );
	$comm = '';
	if ( $comments ) : foreach ( $comments as $comment ) :
		$comm .= '<ul><li><a class="author" href="' . get_permalink( $comment->comment_post_ID ) . '#comment-' . $comment->comment_ID . '">';
		
		//$comm .= get_avatar( $comment->comment_author_email, $avatar_size );
		
		$comm .= '<img class="imgresponsive" src="http://www.yogsutra.com/wp-bdtg/uploads/2017/02/gavater.jpg" alt="avaetr" />'  ;
		
		$comm .= get_comment_author( $comment->comment_ID ) . ':</a> ';
		
		$comm .= '<p>' . strip_tags( substr( apply_filters( 'get_comment_text', $comment->comment_content ), 0, $comment_len ) ) . '...</p></li></ul>';
	endforeach; else :
		$comm .= 'No comments.';
	endif;
	echo $comm;	
}

?>
<?php bg_recent_comments(); ?>	
<?php return ob_get_clean();
});
