<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div id="mainslider" class="mainslider">
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<?php
	$args = array(
        'posts_per_page' => 10,
        'meta_key' => 'meta-checkbox',
        'meta_value' => 'yes'
        );
        $featured = new WP_Query($args); ?>
<ol class="carousel-indicators">
    	<?php
    	for ($i = 0; $i < $featured->post_count; $i++) {
        	if ($i == 0) {
            	$class = 'active';
        	} else {
            	$class = '';
        	}
        	?>
<li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php echo $class; ?>"></li>
    	<?php } ?>
</ol>
<!-- Wrapper for slides -->
<div class="carousel-inner" role="listbox">
   	<?php
	$count = 0;
	if ($featured->have_posts()): while($featured->have_posts()): $featured->the_post();
   	
        	$class = '';
        	if ($count == 0) {
            	$class = 'active';
        	}
    	?>	     
<div class="item <?php echo $class; ?>">
<div class="carosel_img"> 
<a href="<?php the_permalink(); ?>">
		<?php if ( has_post_thumbnail() ) {
                  the_post_thumbnail( 'post', array( 'class'  => 'img-responsive' ) );
                 } 
		?></a>
</div>
<div class="carousel-caption">
<h2 class="entry-title"><a href="<?php the_permalink() ; ?>"> <?php the_title() ; ?></a></h2>  
<br/>
<a href="<?php echo get_permalink(); ?>">Read More</a>
</div>
</div>
<?php $count++;    	
endwhile; else:
endif; ?>
</div>	
</div>
</div>