<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// widget

class yogsutra_Multitab_Widget extends WP_Widget {
    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'yogsutra_Multitab_Widget', // Base ID
            __( 'Popular/Recent post tab', 'yogsutra_title_multitab_widget' ), // Name
            array( 'description' => __( 'popular post / recent post tab', 'yogsutra_title_multitab_widget' ), ) // Args
            );
    }
    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        echo '<div class="yogsutra-tab">
        <ul class="nav  nav-pills nav-justified">
            <li class="active"><a href="#popular" data-toggle="pill"><i class="fa fa-clock-o"></i> Recent Posts</a></li>
            <li><a href="#latest" data-toggle="pill"><i class="fa fa-fire"></i> Popular Posts</a></li>                      
        </ul>';
        echo '<div class="tab-content">';
            echo '<div  class="tab-pane fade in active" id="popular">';
                        query_posts( array( 'posts_per_page' => 5, 'orderby' => 'meta_value_num', 'order' => 'DESC' ) );
                        if (have_posts()) :
                        echo '<ul>';
                        while (have_posts()) :
                            the_post();
			
                                echo '<li>';
									
                            echo'<a href="';
				the_permalink();
							
                            echo'">';

                        if ( has_post_thumbnail() ) {
                  the_post_thumbnail( 'post-img-sidebar', array( 'class'  => 'img-responsive' ) );
                 }

							echo'</a>';
							
							  echo'<p><a href="';
							the_permalink();
							
                            echo'">';

                        the_title();

			echo'</a></p>';
						
                            echo'</li>';
							
                        endwhile;
						
                        echo '</ul>';
                        endif;
                        wp_reset_query();
            echo '</div>';
									
            echo '<div class="tab-pane fade" id="latest">';
                    query_posts( array( 'posts_per_page' => 5, 'orderby' => 'title', 'orderby' => 'rand' ) );
                    if (have_posts()) :
                        echo '<ul>';
                        while (have_posts()) :
                            the_post();
                           echo '<li>';
									
                            echo'<a href="';
							the_permalink();
							
                            echo'">';

                        if ( has_post_thumbnail() ) {
                  the_post_thumbnail( 'post-img-sidebar', array( 'class'  => 'img-responsive' ) );
                 }

							echo'</a>';
							
							  echo'<p><a href="';
							the_permalink();
							
                            echo'">';

                        the_title();

							echo'</a></p>';
						
                            echo'</li>';
							
                        endwhile;
                        echo '</ul>';
                    endif;
                    wp_reset_query();
            echo '</div>';
			
			
        echo '</div>';
    echo '</div>';
    echo $args['after_widget'];
    }
 
    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        echo '<p>Tab content Popular post and recent posts </p>';
    }
    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        return $instance;
    }
 
} // end class yogsutra_Multitab_Widget
function register_yogsutra_Multitab_widget() {
    register_widget( 'yogsutra_Multitab_Widget' );
}
add_action( 'widgets_init', 'register_yogsutra_Multitab_widget' );