/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function($){
    
   "use strict" ;
   
    $(document).ready(function(){
        
          /* search */
    $('.search-open').on('click', function() {
        $('.search-inside').fadeIn();
    });
    $('.search-close').on('click', function() {
        $('.search-inside').fadeOut();
    });
        
   	 // Sticky Menu
    $(".stickynav").sticky({
        topSpacing: 0
    }); 
    
    // click to scroll up and bottom
   
    	$(window).scroll(function() {
            if ($(this).scrollTop() > 200) {
               $('#scroll-to-top').fadeIn('slow');
            } else {
                $('#scroll-to-top').fadeOut('slow');
            }

              if ($(this).scrollTop() > $(document).height() - screen.height) {
               $('#scroll-to-bottom').fadeOut('slow');
            } else {
               $('#scroll-to-bottom').fadeIn('slow');
            }

				 });

                 
     $('.scroll-to-top').click(function(){
       $("html,body").animate({
                        scrollTop:0
                        },600);
                  return false;
      
	});
     $('.scroll-to-bottom').click(function(){
      $('html,body').animate({
          scrollTop: $(document).height()},
              600); 
	return false;
	});
        
      // jQuery Custom scrollbar
		$("body").niceScroll({
			cursorcolor: "#44749A",
			cursorborderradius: "0px",
			cursorwidth: "10px",
			cursorminheight: 100,
			cursorborder: "0px solid #44749A",
			zindex: 9999,
			autohidemode: true,
			horizrailenabled:false
		});
      
    });
    
      
})(jQuery);
