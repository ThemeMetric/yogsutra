<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Yogsutra
 */
global $yoga_opt;
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Yogsutra">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" href="<?php echo $yoga_opt['favicon'][url] ; ?>" /> 
<style type="text/css">
<?php if ( (isset($yoga_opt['custom-css'])) && ($yoga_opt['custom-css'] != "") ) : ?>
<?php echo $yoga_opt['custom-css']; ?>
<?php endif; ?>
</style>
<?php wp_head(); ?>
<?php if ( (isset($yoga_opt['analytics_js'])) && ($yoga_opt['analytics_js'] != "") ) : ?>
<?php echo $yoga_opt['analytics_js']; ?>
<?php endif; ?>
</head>
<body <?php body_class(); ?>>
<div id="page" class="site">
    <header id="masthead" class="site-header">
	<div class="header_top">
	    <div class="container header_top_content">
		<div class="row">		   
		    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 hidden-xs">
			<?php if ( has_nav_menu( 'header_top' ) ) {
                                        wp_nav_menu( array(
                                            'menu'              => 'header_top',
                                            'theme_location'    => 'header_top',
                                            'depth'             => 0,
                                            'container'         => 'div',
                                            'container_class'   => '',
                                            'container_id'      => '',
                                            'menu_class'        => 'nav navbar-nav',
                                            'menu_id'        => 'nav',
                                            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                            'walker'            => new wp_bootstrap_navwalker())
                                        );
			    }
                                      ?>			   			    
		    </div>
		    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 hidden-xs">
			    
			<ul class="nav nav-pills pull-right header_right_contents">
			    <?php if ($yoga_opt['yogsutra-facebook']) :?>
			     <li><a href="<?php echo $yoga_opt['yogsutra-facebook'] ; ?>"><i class="fa fa-facebook"></i></a></li>
			    <?php else : ?>
			    <?php endif ;?>

			    <?php if ($yoga_opt['yogsutra-twitter']) :?>
			    <li><a href="<?php echo $yoga_opt['yogsutra-twitter'] ; ?>"><i class="fa fa-twitter"></i></a></li>
			    <?php else : ?>
			    <?php endif ;?>
			    <?php if ($yoga_opt['yogsutra-linkdin']) :?>
			    <li><a href="<?php echo $yoga_opt['yogsutra-linkdin'] ; ?>"><i class="fa fa-linkedin"></i></a></li>
			    <?php else : ?>
			    <?php endif ;?>
				  <?php if ($yoga_opt['yogsutra-google']) :?>
			    <li><a href="<?php echo $yoga_opt['yogsutra-google'] ; ?>"><i class="fa fa-google-plus"></i></a></li>
			    <?php else : ?>
			    <?php endif ;?>		    
                        </ul> 
		    </div>			
		</div>		    
	    </div>
	</div>
	

    
    
    
    <nav class="stickynav">
        <div class="container mainmenu">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	  
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		</button>
		
		<div class="logo">			
			<?php if($yoga_opt['logo'][url]):?>
				<a href="<?php bloginfo('url');  ?>">
			<img class="img-responsive" src="<?php  echo $yoga_opt['logo'][url] ; ?>" alt="Logo" />            </a>	    
			<?php	else : ?>
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
			<?php endif ;?>		                      
           </div>
		
	    </div>
	    
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            
<div class="search-header">
<a class="search-open" href="javascript:void(0);">
  <i class="fa fa-search"></i>
</a>
<div class="search-inside">
<div class="search-close"><i class="fa fa-times"></i></div>
<div class="search-form">
    <form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
        
        <label class="search-text"> Search For: </label>
        
        <input type="search" class="form-control" placeholder="Type Something" name="s"/>
        <button  class="btn btn-success pull-right" type="submit" value="submit">Submit</button>
    </form>
</div>
</div>	
</div>    
            <?php wp_nav_menu( array(
		'menu'              => 'primary',
		'theme_location'    => 'primary',
		'container'         => 'div',
		'container_class'   => '',
		'container_id'      => '',
		'menu_class'        => 'nav navbar-nav navbar-right',
		'menu_id'        => 'nav',
		'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
		'walker'            => new wp_bootstrap_navwalker())
	    );
	  ?>

        </div>
            




        </div>
    </nav>
    
    <div id="content" class="site-content">