<?php

  /*
  Template Name: Home mobile
  
  */

global $yoga_opt;
get_header(); ?>
<div class="container boda">
    <div class="row">
	<div id="primary" class="content-area">	    	    
	          <main id="main" class="site-main">		    		
                                     
		    <?php  while ( have_posts() ) : the_post();		
                         the_content();
		             endwhile; ?>
	             </div>
                               	    
	  </main>

    </div> 
</div>
<?php get_footer();