<?php
/**
 * Yogsutra functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Yogsutra
 */

if ( ! function_exists( 'yogsutra_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function yogsutra_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Yogsutra, use a find and replace
	 * to change 'yogsutra' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'yogsutra', get_template_directory() . '/languages' );
	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'post', 750, 360, true ); 
	add_image_size( 'related-post', 580, 300, true ); 
	add_image_size( 'post-img-sidebar', 50, 50, true ); 
	
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'yogsutra' ),
		'header_top' => esc_html__( 'Header top', 'yogsutra' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
       	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );
	
	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'yogsutra_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function yogsutra_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'yogsutra' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'yogsutra' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer-1', 'yogsutra' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add footer one widgets here.', 'yogsutra' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<p class="widget-title">',
		'after_title'   => '</p>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer-2', 'yogsutra' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add footer two widgets here.', 'yogsutra' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<p class="widget-title">',
		'after_title'   => '</p>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer-3', 'yogsutra' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'yogsutra' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<p class="widget-title">',
		'after_title'   => '</p>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer-4', 'yogsutra' ),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Add widgets here.', 'yogsutra' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<p class="widget-title">',
		'after_title'   => '</p>',
	) );
}
add_action( 'widgets_init', 'yogsutra_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function yogsutra_scripts() {
                      wp_enqueue_style( 'yogsutra-fonts', yogsutra_fonts_url(), array(), null );	
	wp_enqueue_style( 'yogsutra-bootsrap', get_theme_file_uri( '/assets/css/bootstrap.min.css' ));
	wp_enqueue_style( 'yogsutra-fontawesome', get_theme_file_uri( '/assets/css/font-awesome.min.css' ), array( 'yogsutra-style' ));
	wp_enqueue_style( 'yogsutra-style', get_stylesheet_uri() );

	
	wp_enqueue_script( 'yogsutra-bootstrap', get_theme_file_uri('/assets/js/bootstrap.min.js'), array('jquery'), '', true );
	wp_enqueue_script( 'yogsutra-scripts', get_theme_file_uri('/assets/js/scripts.js'), array('jquery'), '', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'yogsutra_scripts' );

/**
 * Implement the Custom Header feature.
 */
// require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/yogsutra_navwalker.php';
require get_template_directory() . '/inc/widget.php';
require get_template_directory() . '/inc/shortcode.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/******************************************************************************/
/***************************** Theme Options **********************************/
/******************************************************************************/
if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/redux-framework/ReduxCore/framework.php' ) ) {
    require_once( dirname( __FILE__ ) . '/redux-framework/ReduxCore/framework.php' );
}
if ( !isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . '/redux-framework/yogsutra.config.php' ) ) {
    require_once( dirname( __FILE__ ) . '/redux-framework/yogsutra.config.php' );
}
global $yoga_opt;
add_action( 'wp_head', 'wps_params', 10 );
function wps_params() {
    ?>
    
	<script>
	if (window.location.pathname == '/' && jQuery(window).width() <= 640) {
	   window.location = "/mobile/";
	}
	</script>

    <?php
}