<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Yogsutra
 */
get_header(); ?>
<div class="container">
    <div class="row">
	<div id="primary" class="content-area">
	    <main id="main" class="site-main">
                                 <div class="col-md-8 col-lg-8 col-sm-6 col-xs-12">
		    <?php
		    if ( have_posts() ) : ?>
			    <header class="page-header">
				    <?php
					    the_archive_title( '<h1 class="page-title">', '</h1>' );
					    the_archive_description( '<div class="archive-description">', '</div>' );
				    ?>
			    </header><!-- .page-header -->
			    <?php
			    /* Start the Loop */
			    while ( have_posts() ) : the_post();

				    /*
				     * Include the Post-Format-specific template for the content.
				     * If you want to override this in a child theme, then include a file
				     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				     */
				    get_template_part( 'template-parts/content', get_post_format() );

			    endwhile;
			   
		    else :
			    get_template_part( 'template-parts/content', 'none' );

		    endif; ?>
		<div class="post-pagination text-center"> 
		   <?php the_posts_pagination(array(
		     'next_text' => '<span aria-hidden="true">Next <i class="fa fa-angle-right" aria-hidden="true"></i></span>',
		     'prev_text' => '<span aria-hidden="true"> <i class="fa fa-angle-left" aria-hidden="true"></i> Prev </span>',
		     'screen_reader_text' => ' ',
		     'type'                => 'list'
		     )); ?>
		 </div> 	
		 </div>		    
		<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
		    <?php get_sidebar(); ?>
		  </div>
	    </main><!-- #main -->
	</div><!-- #primary -->	
    </div>
</div>
<?php get_footer();
