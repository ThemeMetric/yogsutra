<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Yogsutra
 */

get_header(); ?>  
<div class="container">
    <div class="row">
	<div id="primary" class="content-area">
	           <main id="main" class="site-main">
                                            <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/single-post-content', get_post_format() );

				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			endwhile; 
			?>
                                          </div>		    
		<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">	    
	                                  <?php get_sidebar(); ?>
	                      </div>
	           </main>
	</div>
 </div>   
</div>
<?php get_footer();
