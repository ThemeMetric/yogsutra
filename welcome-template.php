<?php

  /*
  Template Name: Home with slider 
  
  */

global $yoga_opt;
get_header(); ?>
<div class="container">
    <div class="row">
	<div id="primary" class="content-area">	    	    
	    <main id="main" class="site-main">
		    		
                                   <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
	                   <?php get_template_part('inc/slider'); ?>		    			    
		    <?php
			while ( have_posts() ) : the_post();				
                                                                      the_content();			
			endwhile; 
			?>		    		    
	                    <?php wp_reset_postdata(); ?>
                                    </div>
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">	    
	     <?php get_sidebar(); ?>	    
	    </div>		    
	  </main><!-- #main -->
	</div><!-- #primary -->	
    </div>   
</div>
<?php get_footer();