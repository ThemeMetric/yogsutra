<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Yogsutra
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>    		
	<div class="entry-content-page">
		<?php the_content();?>	    
	</div><!-- .entry-content -->	
</article><!-- #post-## -->
