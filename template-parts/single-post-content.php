<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
global $yoga_opt;
?>



<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  

	<header class="entry-header">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;
		 ?>
	</header><!-- .entry-header -->	
     
             <div class="post-media post-single-media">               
	     <a href="<?php the_permalink(); ?>">
	    <?php if ( has_post_thumbnail() ) {
	      the_post_thumbnail( 'post', array( 'class'  => 'img-responsive' ) );
	     } 
		?></a>				
          </div>
          
           
	

	
	
	<div class="post-add">   
 <?php if ( (isset($yoga_opt['single_post_top_add_js'])) && ($yoga_opt['single_post_top_add_js'] != "") ) : ?>
<?php echo $yoga_opt['single_post_top_add_js']; ?>
<?php endif; ?> 
</div>
	

		
	<div class="entry-content">	    
	   <?php the_content(); ?>	   	
	</div><!-- .entry-content -->
	
		<div class="entry-cat hidden-xs">	    
	    <ul class="nav nav-pills nav-justified">
		<li><a href="<?php echo site_url(); ?>/author/<?php the_author(); ?>"><i class="fa fa-user"></i> <?php the_author(); ?></a></li>
		<li><a href="#"><i class="fa fa-calendar"></i> <?php the_time('d/m/Y'); ?></a></li>		
		<li><a href="#reply-title"><i class="fa fa-comments"> </i> Leave a comment</a></li>		
		</ul>	   
	</div><!-- .entry-footer -->
	<br/>
	
	<footer class="entry-footer-tags hidden-xs">
	    <div class="entry-footer-tagss">
	 <p><i class="fa fa-tag"></i>  <?php the_tags( ' ', ', ', '<br />' ); ?> </p>    	
	    </div>    	           		
	</footer>
			
	<?php if ($yoga_opt['opt-switch']) :?>
    <?php $orig_post = $post;
    global $post;
    $categories = get_the_category($post->ID);
    if ($categories) {
    $category_ids = array();
    foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
    $args=array(
    'category__in' => $category_ids,
    'post__not_in' => array($post->ID),
    'posts_per_page'=> 3,
    'orderby'             => 'rand',
    'caller_get_posts'=>1
    );
    $my_query = new wp_query( $args );
    if( $my_query->have_posts() ) {
    echo '<div id="related_posts"><h3>Related Posts</h3>';
    while( $my_query->have_posts() ) {
    $my_query->the_post(); ?> 

	<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">	     
	   <div class="relatedthumb">
            <a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) {
       the_post_thumbnail( 'related-post', array( 'class'  => 'img-responsive img-thumbnail' ) );
      } ?></a>
           </div>
	    <div class="relatedcontent">
	    <h5><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h5>  
	    </div>
	</div>	
    <?php
    }
    echo '</div>';
    }
    }
    $post = $orig_post;
    wp_reset_query(); ?>
	
	<?php else : ?>

	<?php endif ;?>
      
			
</article>

<div class="post-add">   
<?php if ( (isset($yoga_opt['single_post_bottom_add_js'])) && ($yoga_opt['single_post_bottom_add_js'] != "") ) : ?>
<?php echo $yoga_opt['single_post_bottom_add_js']; ?>
<?php endif; ?>
</div>
