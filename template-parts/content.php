<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Yogsutra
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>     
  
  
  
  <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
     <div class="post-media">               
	     <a href="<?php the_permalink(); ?>">
		<?php if ( has_post_thumbnail() ) {
                  the_post_thumbnail( 'post', array( 'class'  => 'img-responsive img-thumbnail' ) );
                 } 
		?></a>				
          </div> 
  </div>
    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
               
	<header class="entry-header">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		 ?>
	</header><!-- .entry-header -->
	
	<div class="entry-content entry-content-link-button">	    
	   <?php the_excerpt(); ?>	    
	   <a href="<?php echo get_permalink(); ?>">Read More</a>	    	   	
	</div><!-- .entry-content -->
  </div>
  
	<footer class="entry-footer hidden-xs">	    
	    <ul class="nav nav-pills nav-justified">
		<li><a href="<?php echo site_url(); ?>/author/<?php the_author(); ?>"><i class="fa fa-user"></i> <?php the_author(); ?></a></li>
		<li><a href="#"><i class="fa fa-calendar"></i> <?php the_time('d/m/Y'); ?></a></li>		
		<li><?php $categories = get_the_category();
if ( ! empty( $categories ) ) {
    echo '<a href="'.esc_url( get_category_link( $categories[0]->term_id ) ).'"><i class="fa fa-wpexplorer"></i> '.esc_html( $categories[0]->name ).'</a>';
} ?></li></ul>	   
	</footer><!-- .entry-footer -->
	
</article><!-- #post-## -->